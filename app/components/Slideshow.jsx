import React from 'react';
import PropTypes from 'prop-types';
import BackgroundImgs from './BackgroundImgs';
import AlbumImgs from './AlbumImgs';
import Bars from './Bars';


const divStyle = {
  background: 'rgba(0, 0, 0, 0) radial-gradient(circle at center top, rgb(72, 127, 157), rgb(23, 86, 120) 60%) repeat scroll 0% 0%',
};
const spanStyle = {
  background: 'rgb(88, 211, 225) none repeat scroll 0% 0%',
};
const h2Syule = {
  color: 'rgb(88, 211, 225)',
};
const spanBlockStyle = {
  display: 'block',
};

class Slideshow extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isHovered: [false, false, false, false], isHover: 0 };

    this.hoverHandler = this.hoverHandler.bind(this);
    this.mouseOutHandler = this.mouseOutHandler.bind(this);
  }

  hoverHandler(id) {
    const array = this.state.isHovered;
    array[id] = true;
    this.setState({ isHovered: array, isHover: id + 1 });
  }

  mouseOutHandler(id) {
    const array = this.state.isHovered;
    array[id] = false;
    this.setState({ isHovered: array, isHover: 0 });
  }

  renderImage(imageUrl, number) {
    return (
      <BackgroundImgs key={number} imageUrl={imageUrl} isHovered={this.state.isHovered[number]} />
    );
  }

  renderAlbums(albumImageUrl, number) {
    return (
      <AlbumImgs
        key={number}
        keys={number}
        albumImageUrl={albumImageUrl}
        albumTitle={this.props.albumTitle[number]}
        songTitle={this.props.songTitle[number]}
        hoverHandler={this.hoverHandler}
        mouseOutHandler={this.mouseOutHandler}
        isHovered={this.state.isHovered[number]}
        isHover={this.state.isHover}
      />
    );
  }

  render() {
    return (
      <div
        className="c-volume u-flex u-justify-content-c u-bg--brand u-fit u-fit-w is-load"
        style={divStyle}
      >
        <div className="c-volume-slideshow u-absolute u-pos-tl u-fit u-pointer-none">
          {this.props.imageUrls.map((imageUrl, index) => this.renderImage(imageUrl, index))}
        </div>
        <div className="is-no-drag c-volume-slider u-viewport-fit-h
        u-relative u-fit-w u-flex u-align-items-c u-select-none u-overflow-h"
        >
          <div className="c-volume-head u-absolute u-pos-tl u-fit-w
          u-text-center u-marg-t-lg u-pointer-none | u-marg-t-xl@md"
          >
            <h1 className="c-volume-head-line c-volume-head-line--1 t-h1 u-color--white">Vol.1</h1>
            <span className="c-volume-head-line c-volume-head-line--1
            c-line u-inline-block u-marg-y-xs"
            >
              <span className="c-line-1" style={spanStyle} />
              <span className="c-line-2" style={spanStyle} />
              <span className="c-line-3" style={spanStyle} />
            </span>
            <h2
              className="c-volume-head-line c-volume-head-line--2
            t-h6 u-capitalize"
              style={h2Syule}
            >
              Oct - Dec 2017
            </h2>
            <h2 className="c-volume-head-line c-volume-head-line--3
            c-volume-legend t-text--xl u-medium u-color--white
            u-marg-t-md u-marg-x-auto"
            >
              <p>Get ready for VOL. 1, the latest Air France Music selection.
                <span style={spanBlockStyle}>Playing now on board Air France.</span>
              </p>
            </h2>
          </div>
          <div className="c-volume-inner u-fit-w u-relative
          u-fit-w u-white-space-nowrap u-pad-x-xxl"
          >
            {this.props.albumImageUrls.map((albumImageUrl, index) =>
                this.renderAlbums(albumImageUrl, index))}
          </div>
          <Bars />
        </div>
        <a
          href="/en/player"
          className="c-volume-player-btn t-btn u-absolute u-pos-tr
         u-marg-t-lg u-flex u-justify-content-c u-align-items-c | u-hide@sm"
        >
          <svg
            version="1.1"
            id="Calque_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            viewBox="0 0 17 10"
            xmlSpace="preserve"
            width="17"
            height="10"
            className="u-fill--white"
          >
            <path d="M16,2H9C8.4,2,8,1.6,8,1v0c0-0.5,0.4-1,1-1h7c0.5,0,
            1,0.4,1,1v0C17,1.6,16.6,2,16,2z"
            />
            <path d="M16,6H9C8.4,6,8,5.6,8,5v0c0-0.5,0.4-1,1-1h7c0.5,0,
            1,0.4,1,1v0C17,5.6,16.6,6,16,6z"
            />
            <path d="M16,10H9c-0.5,0-1-0.4-1-1v0c0-0.5,0.4-1,1-1h7c0.5,0,
            1,0.4,1,1v0C17,9.6,16.6,10,16,10z"
            />
            <polygon points="0,0 5,5 0,10 " />
          </svg>
          <span className="c-volume-player-btn-text t-text--xs u-uppercase u-bold
          u-color--white u-marg-l-xs"
          >Listening mode
          </span>
        </a>
      </div>
    );
  }
}

Slideshow.propTypes = {
  imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
  albumImageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
  albumTitle: PropTypes.arrayOf(PropTypes.string).isRequired,
  songTitle: PropTypes.arrayOf(PropTypes.string).isRequired,
};
export default Slideshow;
