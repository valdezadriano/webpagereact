import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const imgStyle = {
  width: '1600px',
  height: '853px',
  transform: 'translate(0px, -32.5px) translateZ(0px)',

};

class BackgroundImgs extends React.Component {
  render() {
    const classes = classNames('c-volume-slide u-absolute u-pos-tl u-fit u-force-3d u-overflow-h', {
      'is-next': !this.props.isHovered,
      'is-active': this.props.isHovered,
    });

    return (
      <div
        className={classes}
        onMouseOver={this.handleHover}
        onFocus={this.handleHover}
        onMouseOut={this.handleMouseOut}
        onBlur={this.handleMouseOut}
      >
        <div />
        <div className="u-block u-absolute u-pos-tl u-fit">
          <img
            alt="background album"
            className="js-object-fit u-absolute u-pos-tl u-fit-w u-force-3d"
            draggable={false}
            src={this.props.imageUrl}
            style={imgStyle}
          />
        </div>
      </div>
    );
  }
}


BackgroundImgs.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  isHovered: PropTypes.bool.isRequired,
};

export default BackgroundImgs;
